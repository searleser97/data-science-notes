% run the command ' lualatex -shell-escape Notes.tex ' twice in the terminal to visualize table of contents
\documentclass[twoside]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{geometry}
\usepackage{multicol}
\usepackage{minted}
\usepackage{python}
\usepackage[hidelinks]{hyperref}
\usepackage{fancyhdr}
\usepackage{listings}
\usepackage{pdfpages}
\usepackage{needspace}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{ragged2e}
\usepackage{placeins}
\usepackage{parskip}
\usepackage{amsmath}
\usepackage{titlesec}

\newcommand{\wdir}[1]{/home/san/Videos/Courses/EDX/Data Science/Notes/#1}

\geometry{letterpaper, portrait, left=1cm, right=1cm, top=1.8cm, bottom=1.5cm}

\sectionfont{\Huge\bfseries\sffamily}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}
\renewcommand{\indent}{\hspace*{2em}}

\pagestyle{fancy}
\pagenumbering{arabic}
\fancyhead{}
\fancyfoot{}
\fancyhead[LE,RO]{\textsf{First, solve the problem. Then, write the code.}}
\fancyhead[LO,RE]{\textsf{\leftmark}}
\fancyfoot[LO,RE]{\textbf{\textsf{\thepage}}}
 
\renewcommand{\headrulewidth}{0.01cm}
\renewcommand{\footrulewidth}{0.01cm}

% Define light and dark Microsoft blue colours
\definecolor{MSBlue}{rgb}{.204,.353,.541}
\definecolor{MSLightBlue}{rgb}{.02,.541,.325}

\titleformat*{\section}{\Large\bfseries\sffamily\color{MSBlue}}
\titleformat*{\subsection}{\large\bfseries\sffamily\color{MSLightBlue}}
 
\begin{document}
\includepdf{TitlePage.pdf}

\fontfamily{lmss}
\selectfont
\tableofcontents
\newpage

<<echo=FALSE>>=
opts_chunk$set(background = "#f2f2f2", message = FALSE)
@

<<child="2 Data Visualization/DataVisualization.Rnw">>=
@

\end{document}